from setuptools import setup, find_packages

setup(
    name='teddy_lazebnik',
    version='0.1.0',
    url='https://bitbucket.org/teddy_lazebnik/knn_over_time',
    license='MIT2',
    author='teddy lazebnik',
    author_email='lazebnik.teddy@gmail.com',
    description='A project contains a Knn over time. A new version on Knn algorithm which taking time as a process '
                'and not as a feature, to provide analysis of predictions cluster over discrete time',
    packages=find_packages(),
    install_requires=[
        'pillow==5.4.1',
        'matplotlib==3.0.2',
        'numpy==1.16.1',
        'pandas==0.25.3',
        'scipy==1.3.2',
        'scikit-learn==0.21.3',
        'seaborn==0.9.0'
    ],
    dependency_links=[
    ],
    scripts=[
    ]
)
