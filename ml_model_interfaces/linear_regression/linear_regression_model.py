# library imports
import os
import json
import numpy
from sklearn.linear_model import LinearRegression

# projects imports
from ml_model_interfaces.ml_model.ml_model import IMlModel
from ml_model_interfaces.save_load_model import ModelSaverLoader


class LinearRegressionModel(IMlModel):
    """ Linear Regression Model """

    def __init__(self):
        IMlModel.__init__(self)
        self._model = None
        self._r_value_squre = None

    # ---> ml operation <--- #

    def train(self, x: list, y: list):
        """ run linear regression and """
        self._model = LinearRegression().fit(X=numpy.array(x), y=numpy.array(y))
        self._r_value_squre = self._model.score(x, y)

    def predict(self, x: list):
        """ given an input point, find the y-value of it using the linear regression """
        return self._model.predict(numpy.array(x))[0]

    def predict_list(self, x: list):
        """ given an list of input point, find the y-value of it using the linear regression """
        return [self.predict(i) for i in x]

    def get_results(self, parms=[]) -> dict:
        """ get list of parms intresting to save, compare"""
        return {"slope": self._model.coef_,
                "intercept": self._model.intercept_,
                "r_squre": self._r_value_squre}

    # ---> end - ml operation <--- #

    # ---> model versions <--- #

    @staticmethod
    def save_model_results(path: str, parms: dict) -> None:
        """ save a model results """
        # make sure we are good to go'
        if not (path is not None and os.path.isfile(path) and 0 <= parms["r_squre"] <= 100):
            raise Exception("arguments are not legit")
        # write it
        with open(path, 'w') as file:
            file.write(json.dumps(parms))

    @staticmethod
    def load_model_results(path: str):
        """ load a model results """
        # make sure we are good to go
        if not (path is not None and os.path.isfile(path)):
            raise Exception("path to file is not legit")
        # write it
        with open(path, 'r') as file:
            return json.load(file)

    @staticmethod
    def compare_model_results(path: str, parms: dict) -> bool:
        """ load model and compare to the database for the new one """
        # make sure we are good to go
        if not (path is not None and os.path.isfile(path)):
            raise Exception("arguments are not legit")
        # load all results
        old_results = LinearRegressionModel.load_model_results(path)
        # tell if better or not
        return True if parms["r_squre"] >= old_results["r_squre"] else False

    # ---> end - model versions <--- #

    # ---> getters <--- #

    def get_slope(self) -> float:
        return self._slope

    def get_intercept(self) -> float:
        return self._slope

    def get_r_value_squre(self) -> float:
        return self._r_value_squre

    # ---> end - getters <--- #

    # ---> model operation <--- #

    def save_file(self, file_path: str) -> None:
        """ get the parms of the model into file"""
        return ModelSaverLoader.save_model(path_to_file=file_path,
                                           model={"slope": self._slope,
                                                  "intercept": self._intercept,
                                                  "r_squre": self._r_value_squre})

    @staticmethod
    def load_file(file_path: str) -> object:
        """ use parms to load model of LogisticRegression """
        return ModelSaverLoader.load_model(path_to_file=file_path)

    # ---> end - model operation <--- #
