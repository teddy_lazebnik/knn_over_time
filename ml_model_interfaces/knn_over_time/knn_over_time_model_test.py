# library imports
import unittest

# projects imports
from ml_model_interfaces.knn_over_time.knn_over_time_model import KnnTimeModel


class KnnTimeModelTest(unittest.TestCase):
    """ knn with time Model - unit test """

    error_rate = 0.0001

    def test_0(self):
        k = 1
        x = [[[1], [10], [1]]]
        y = [[1, 1, 1]]
        model = KnnTimeModel(k=k,
                             poly_degree=1)
        model.train(x=x, y=y)
        prediction = model.predict([1], 3)
        gt = 1
        self.assertAlmostEqual(abs(prediction - gt), KnnTimeModelTest.error_rate)

    def test_1(self):
        k = 1
        x = [[[1], [2], [3]]]
        y = [[1, 2, 3]]
        model = KnnTimeModel(k=k,
                             poly_degree=1)
        model.train(x=x, y=y)
        prediction = model.predict([4], 3)
        gt = 4
        self.assertAlmostEqual(abs(prediction - gt), KnnTimeModelTest.error_rate)

    def test_2(self):
        k = 2
        x = [[[1], [2], [3]], [[1], [1], [1]]]
        y = [[1, 2, 3], [1, 1, 1]]
        model = KnnTimeModel(k=k,
                             poly_degree=1)
        model.train(x=x, y=y)
        prediction = model.predict([1], 4)
        gt = 3
        self.assertAlmostEqual(abs(prediction - gt), KnnTimeModelTest.error_rate)

    def test_3(self):
        k = 3
        x = [[[2], [2], [2]], [[1], [1], [1]], [[1], [1], [1]], [[-1], [0], [1]], [[5], [5], [5]]]
        y = [[2, 2, 2], [1, 1, 1], [3, 3, 3], [-2, 0, 2], [5, 5, 5]]
        model = KnnTimeModel(k=k,
                             poly_degree=1)
        model.train(x=x, y=y)
        prediction = model.predict([2], 3)
        prediction2 = model.predict([5], 2)
        gt = 2
        gt2 = 4
        self.assertAlmostEqual(abs(prediction - gt), KnnTimeModelTest.error_rate)
        self.assertAlmostEqual(abs(prediction2 - gt2), KnnTimeModelTest.error_rate)


if __name__ == '__main__':
    unittest.main()