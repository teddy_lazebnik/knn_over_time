# library imports
import math
import numpy

# projects imports
from ml_model_interfaces.linear_regression.linear_regression_model import LinearRegressionModel


class KnnTimeModel:
    """ knn with time Model """

    def __init__(self, k: int, poly_degree: int = 1):
        # the number of points to KNN
        if k < 1:
            self._k = 1
        else:
            self._k = k
        # the poly extrapolation degree
        if poly_degree < 1:
            self._poly_degree = 1
        else:
            self._poly_degree = poly_degree
        self._model = None
        self._is_trained = False

    # ---> ml operation <--- #

    def train(self, x: list, y: list, start_time=0, time_step=1):
        """ train random forest model:
            x - list of list of list as list of vector of points (presented as list)
            y - list of list of list as list of vector of points (presented as list)
            start_time - the time we wish to start the learning
            time_step - the step if we don't wish it to be 1
            we think on each point as a list of points over time converting the damnation from n to (n+1) cause adding time.
            The idea is to approximate each list of dots by some function and each new point in time and space as an approximation
            using simple AVG or weighted AVG by distance """
        # make sure calc will make logic
        if time_step < 0:
            time_step = 1.0
        # convert list of list of list to list of models
        self._model = [self._approximate_points_to_line(points_x=x[time_list_index],
                                                        points_y=y[time_list_index],
                                                        start_time=start_time,
                                                        time_step=time_step)
                       for time_list_index in range(len(x))]
        self._is_trained = True

    def predict(self, x: list, time: float, p=2, weighted=False) -> float:
        """ run prediction on all the models, get dot per each one in space X
            and find the ddistanceto the 'k' closes points to get y """
        # make sure we can use the model
        if not self._is_trained:
            raise Exception("model is not trained")
        # project the space from (x,t) to (x) by putting time
        predicted_models_points = [model.predict(x=[[time]]).tolist() for model in self._model]
        decompsed_list = [(point[:-1], point[-1]) for point in predicted_models_points]
        data_x = [decompsed_item[0] for decompsed_item in decompsed_list]
        data_y = [decompsed_item[1] for decompsed_item in decompsed_list]

        # calc dists with some metric as normal knn
        dists = [KnnTimeModel.lp(point1=data_x_item,
                                 point2=x,
                                 p=p)
                 for data_x_item in data_x]

        # get the dists with 'y' values
        dists_value = [(dists[index], data_y[index]) for index in range(len(dists))]
        # sort the list by distnaces
        sorted(dists_value, key=lambda val: val[0])

        # if weighted avg or simple one
        if not weighted:
            # simple formula
            return sum([dists_value[index][1] for index in range(self._k)]) / self._k
        else:
            # weighted formula
            dists = [1 / dists_value[index][0] for index in range(len(dists_value)) if dists_value[index] != 0]
            return sum([dists_value[index][1] * dists[index] for index in range(len(dists_value)) if dists != 0]) / sum(dists)

    def predict_list(self, x: list, time: float) -> list:
        """ run predict on multiple dots in the same time """
        return [self.predict(x=x_item, time=time) for x_item in x]

    def _approximate_points_to_line(self, points_x: list, points_y: list, start_time: float, time_step: float) -> LinearRegressionModel:
        """ approximate a list of points to a single 'stupid' function over space (DIM: N) and time (DIM: 1) """
        # organize new set to N+1 dim and 'y' at time ('t')
        [points_x[index].append(points_y[index]) for index in range(len(points_y))]
        points_y = [[start_time + index * time_step] for index in range(len(points_y))]
        # train a model in the form y = a * t + b
        answer = LinearRegressionModel()
        answer.train(x=points_y,
                     y=points_x)
        return answer

    def _approximate_points_to_poly(self, points_x: list, points_y: list, start_time: float, time_step: float) -> list:
        """ approximate a list of points to a single 'stupid' function over space (DIM: N) and time (DIM: 1) """
        # organize new set to N+1 dim and 'y' at time ('t')
        [points_x[index].append(points_y[index]) for index in range(len(points_y))]
        points_y = [[start_time + index * time_step] for index in range(len(points_y))]
        # train a model in the form y = a0 * t^d + a1 * t^(d-1) + ... + a_(d-1) * t ^ 1 + a_d
        return list(numpy.polyfit(points_x, points_y, self._poly_degree))

    def _poly_vector_assignment(self, factors: list, value: float):
        """
        given a vector of factors for a polynomoal and calc the value
        :param factors: factors of the poly
        :param value: the value to assign to the poly
        :return: the value of the poly in the value
        """
        return sum([factor * math.pow(value, len(factors) - (1 + index))
                    for index, factor in enumerate(factors)])

    @staticmethod
    def lp(point1: list, point2: list, p: int) -> float:
        if p >= 1 and len(point1) == len(point2) and len(point1) > 0:
            return math.pow(sum([math.pow(point1[index] - point2[index], p) for index in range(len(point1))]), 1/p)
        else:
            raise Exception("Cannot work with p less then 1 and not int")

    # ---> end - ml operation <--- #
