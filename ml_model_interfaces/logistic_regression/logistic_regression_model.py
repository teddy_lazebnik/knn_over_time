# library imports
import os
import json
from sklearn.linear_model import LogisticRegression

# projects imports
from ml_model_interfaces.ml_model.ml_model import IMlModel
from ml_model_interfaces.save_load_model import ModelSaverLoader


class LogisticRegressionModel(IMlModel):
    """ Logistic Regression Model """

    def __init__(self):
        IMlModel.__init__(self)
        self._model = LogisticRegressionModel._build()
        self._trained = False

    @staticmethod
    def _build():
        """ build logistic regression instance"""
        return LogisticRegression(penalty='l2',
                                  random_state=15487469,
                                  tol=0.001,
                                  solver='liblinear',
                                  multi_class='ovr',
                                  warm_start=False)

    # ---> ml operation <--- #

    def train(self, x: list, y: list):
        """
        Train logistic regression instance
        :param x: the objects
        :param y: the labels
        :return: trained model
        """
        self._trained = True
        return self._model.fit(x, y)

    def predict(self, x: object):
        """ used the Logistic Regression Model """
        # make sure we are trained
        if self._trained:
            return self._model.predict([x])[0]
        return None

    def predict_list(self, x: list):
        """ used the Logistic Regression Model """
        # make sure we are trained
        if self._trained:
            return self._model.predict(x)
        return None

    def get_results(self, parms=[]) -> dict:
        """ get list of parms intresting to save, compare"""
        if isinstance(parms, list) and len(parms) == 3:
            return {"train": parms[0], "test": parms[1], "acc": parms[2]}
        raise Exception("params is not list in the right size")

    # ---> end - ml operation <--- #

    # ---> model versions <--- #

    @staticmethod
    def save_model_results(path: str, parms: dict):
        """ save a model results """
        # make sure we are good to go
        if not (path is not None and os.path.isfile(path) and parms["train"] > 0 and parms["test"] > 0
                and 0 <= parms["acc"] <= 100):
            raise Exception("arguments are not legit")
        # write it
        with open(path, 'w') as file:
            file.write(json.dumps(parms))

    @staticmethod
    def load_model_results(path: str):
        """ load a model results """
        # make sure we are good to go
        if not (path is not None and os.path.isfile(path)):
            raise Exception("arguments are not legit")
        # write it
        with open(path, 'r') as file:
            return json.load(file)

    @staticmethod
    def compare_model_results(path: str, parms: dict):
        """ load model and compare to the database for the new one """
        # make sure we are good to go
        if not (path is not None and os.path.isfile(path) and parms["train"] > 0 and parms["test"] > 0):
            raise Exception("arguments are not legit")
        old_results = LogisticRegressionModel.load_model_results(path)
        return True if parms["train"] >= old_results["train"] and parms["test"] >= old_results["test"] and parms["acc"] > old_results["acc"] else False

    # ---> end - model versions <--- #

    # ---> model operation <--- #

    def save_file(self, file_path: str) -> None:
        """ get the parms of the model into file"""
        return ModelSaverLoader.save_model(path_to_file=file_path,
                                           model=self._model)

    @staticmethod
    def load_file(file_path: str) -> object:
        """ use parms to load model of LogisticRegression """
        return ModelSaverLoader.load_model(path_to_file=file_path)

    # ---> end - model operation <--- #


