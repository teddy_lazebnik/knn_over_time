from ml_model_interfaces.xmean.k_mean import Kmeans


class Xmeans:
    """ a Xnn algorithm for a given image context """

    def __init__(self,
                 n_init=20,
                 max_iter=300,
                 tol=1e-4,
                 n_jobs=None,
                 algorithm='auto'):
        self._n_init = n_init
        self._max_iter = max_iter
        self._tol = tol
        self._n_jobs = n_jobs

        if algorithm in ["auto", "full" , "elkan"]:
            self._algorithm = algorithm
        else:
            self._algorithm = "auto"

    def run_chosen_k(self, points: list, choosen_k: int) -> Kmeans:
        return self.run(points=points,
                        start_k=choosen_k,
                        end_k=choosen_k,
                        step=1)

    def run(self, points: list, start_k=1, end_k=5, step=1, need_print=False) -> Kmeans:
        score_memory = []
        model_memory = []
        # run over all the sample size
        for i in range(1 + int((end_k-start_k)/step)):
            try:
                # get the next "k" to check
                k = i * step + start_k
                # init database to test
                test_k_set = Kmeans(k=k,
                                    n_init=self._n_init,
                                    max_iter=self._max_iter,
                                    tol=self._tol,
                                    n_jobs=self._n_jobs)
                # run test
                model, new_accuracy = test_k_set.run(points)
                score_memory.append(new_accuracy)
                model_memory.append(model)
                # print info if asked
                if need_print:
                    print("Result at k = {}, mean of mili r^2 = {}".format(k, int(new_accuracy/100)/10))
            except Exception as error:
                print("{}".format(error))
                break
        # find the mean score
        mean_score = sum(score_memory)/len(score_memory)
        # run over the scores
        for score_index in range(len(score_memory)):
            # the first score passes the mean is a shift point and in general the best k for Xmean algorithm
            if score_memory[score_index] < mean_score:
                print("Chosen:\nBest at k = {}, mean of mili r^2 = {}".format(score_index+1, int(score_memory[score_index]/100)/10))
                return model_memory[score_index]
        # take the middle one
        return model_memory[int(len(model_memory)/2)]
