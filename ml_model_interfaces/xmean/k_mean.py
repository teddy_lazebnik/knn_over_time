# library imports
import math
from sklearn.cluster import KMeans

# project imports
from math_ops.distances import Distances


class Kmeans:
    """ run Knn algorithm for a given image context """

    def __init__(self, k: int,
                 n_init=20,
                 max_iter=300,
                 tol=1e-4,
                 n_jobs=None,
                 algorithm='auto'):
        if k < 1:
            self._k = 1
        else:
            self._k = k

        self._n_init = n_init
        self._max_iter = max_iter
        self._tol = tol
        self._n_jobs = n_jobs

        if algorithm in ["auto", "full" , "elkan"]:
            self._algorithm = algorithm
        else:
            self._algorithm = "auto"

    def run(self, points: list):
        # train model
        k_means = KMeans(n_clusters=self._k,
                         n_init=self._n_init,
                         max_iter=self._max_iter,
                         tol=self._tol,
                         n_jobs=self._n_jobs,
                         algorithm=self._algorithm)

        if self._k < len(points):
            k_means.fit(points)
        else:
            raise Exception("Can not work on more classes ({}) then points ({})".format(self._k, len(points)))
        # Predicting the clusters
        labels = k_means.predict(points)
        # calc accuracy
        return k_means, Kmeans.calc_dist_squre_error(k_means.cluster_centers_, labels, points)

    @staticmethod
    def calc_dist_squre_error(centers: list, labels: list, points: list) -> float:
        answer = 0
        for point_index in range(len(points)):
            answer += Distances.l2(centers[labels[point_index]], points[point_index])
        return answer
