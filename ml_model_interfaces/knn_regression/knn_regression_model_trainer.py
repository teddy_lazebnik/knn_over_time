# library imports
from sklearn import metrics

# projects imports
from ml_model_interfaces.ml_model.ml_model_trainer import IMlModelTrainer
from ml_model_interfaces.knn_regression.knn_regression_model import KnnRegressionModel


class KnnRegressionModelTrainer(IMlModelTrainer):
    """ auto trainer to KnnRegressionModel model """

    def __init__(self):
        IMlModelTrainer.__init__(self)

    @staticmethod
    def train_model(save_model_results_path: str,
                    save_model_path: str,
                    x_train: list,
                    y_train: list,
                    x_test: list,
                    y_test: list,
                    save_anyway=False):
        """ open an naive bayes object, prepare database and train it. Test results, and save the better model """
        try:
            print("Start process")
            # build model
            model = KnnRegressionModel()
            # train model
            model.train(x=x_train, y=y_train)
            # check performance
            print("Run on test")
            y_pred = model.predict_list(x=x_test)

            # TODO: replace with regression acc checking
            accuracy = metrics.accuracy_score(y_test, y_pred)
            print("Model ML Accuracy: {:.2f}".format(accuracy*100))
            if save_anyway or model.compare_model_results(path=save_model_results_path,
                                                          parms=model.get_results([accuracy])):
                # save model
                print("Save model to {}".format(save_model_path))
                model.save_file(file_path=save_model_path)
                # save model results
                print("Save model results to {}".format(save_model_results_path))
                model.save_model_results(path=save_model_results_path,
                                         parms=model.get_results([accuracy]))
            else:
                # tell that we won't save
                print("Did not save model results to {}, last model is better".format(save_model_results_path))
            print("End process")
        except Exception as error:
            print("Error at train_model from pre_classifier/model_trainer with sub error: " + str(error))

