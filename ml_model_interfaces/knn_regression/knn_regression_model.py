# library imports
import os
import json
import pickle
from sklearn.neighbors import KNeighborsClassifier

# projects imports
from ml_model_interfaces.ml_model.ml_model import IMlModel
from ml_model_interfaces.save_load_model import ModelSaverLoader


class KnnRegressionModel(IMlModel):
    """ knn with regression Model """

    def __init__(self,
                 k=5,
                 weights='uniform',
                 algorithm='auto',
                 leaf_size=30,
                 p=2,
                 metric='minkowski',
                 metric_params=None,
                 n_jobs=None):
        IMlModel.__init__(self)
        self._model = KNeighborsClassifier(n_neighbors=k,
                                           weights=weights,
                                           algorithm=algorithm,
                                           leaf_size=leaf_size,
                                           p=p,
                                           metric=metric,
                                           metric_params=metric_params,
                                           n_jobs=n_jobs)
        self._k = k
        self._x = []
        self._y = []

    @staticmethod
    def load(model: object):
        answer = KnnRegressionModel()
        answer._model = model
        return answer

    # ---> ml operation <--- #

    def train(self, x: list, y: list):
        """ train random forest model """
        self._model.fit(x, y)
        self._x = x
        self._y = y

    def predict(self, x: list) -> list:
        """ given an input vector, find the right value """
        return sum([self._y[index]
                    for index in self._model.kneighbors(X=[x],
                                                        n_neighbors=self._k,
                                                        return_distance=False).array()]
                   ) / self._k

    def predict_classify(self, x: list) -> list:
        """ given an input vector, find the right value """
        return Counter([self._y[index] for index in self._model.kneighbors(X=[x],
                                                                           n_neighbors=self._k,
                                                                           return_distance=False).array()]).most_common()[0][0]

    def predict_list(self, x: list) -> list:
        """ given an input of list of vectors, find the right value """
        if isinstance(x, list) and len(x) > 0 and isinstance(x[0], list):
            return [self.predict(x=item) for item in x]
        raise Exception("Cannot use the data format provided - should be list of lists of object")

    def get_results(self, parms=[]) -> dict:
        """ get list of parms intresting to save, compare"""
        if len(parms) == 1:
            return {"acc": parms[0]}
        raise Exception("Provide a single 'acc' parm")

    # ---> end - ml operation <--- #

    # ---> model versions <--- #

    @staticmethod
    def save_model_results(path: str, parms: dict) -> None:
        """ save a model results """
        # make sure we are good to go'
        if not (path is not None and os.path.isfile(path) and 0 <= parms["acc"] <= 100):
            raise Exception("arguments are not legit")
        # write it
        with open(path, 'w') as file:
            file.write(json.dumps(parms))

    @staticmethod
    def load_model_results(path: str):
        """ load a model results """
        # make sure we are good to go
        if not (path is not None and os.path.isfile(path)):
            raise Exception("path to file is not legit")
        # write it
        with open(path, 'r') as file:
            return json.load(file)

    @staticmethod
    def compare_model_results(path: str, parms: dict) -> bool:
        """ load model and compare to the database for the new one """
        # make sure we are good to go
        if not (path is not None and os.path.isfile(path)):
            raise Exception("arguments are not legit")
        # load all results
        old_results = KnnRegressionModel.load_model_results(path)
        # tell if better or not
        return True if parms["acc"] >= old_results["acc"] else False

    # ---> end - model versions <--- #

    # ---> getters <--- #

    # ---> end - getters <--- #

    # ---> model operation <--- #

    def save_file(self, file_path: str) -> None:
        """ get the parms of the model into file"""
        return ModelSaverLoader.save_model(path_to_file=file_path,
                                           model=pickle.dumps(self._model))

    @staticmethod
    def load_file(file_path: str) -> object:
        """ use parms to load model of LogisticRegression """
        return KnnRegressionModel.load(ModelSaverLoader.load_model(path_to_file=file_path))

    # ---> end - model operation <--- #
