# library imports
import os
import json
import pickle
from sklearn.ensemble import RandomForestClassifier

# projects imports
from ml_model_interfaces.ml_model.ml_model import IMlModel
from ml_model_interfaces.save_load_model import ModelSaverLoader


class RandomForestModel(IMlModel):
    """ random forest Model """

    def __init__(self,
                 n_estimators='warn',
                 criterion="gini",
                 max_depth=None,
                 min_samples_split=2,
                 min_samples_leaf=1,
                 min_weight_fraction_leaf=0.,
                 max_features="auto",
                 max_leaf_nodes=None,
                 min_impurity_decrease=0.,
                 min_impurity_split=None,
                 bootstrap=True,
                 oob_score=False,
                 n_jobs=None,
                 random_state=None,
                 warm_start=False,
                 class_weight=None,
                 is_train=False):
        IMlModel.__init__(self)
        self._model = RandomForestClassifier(
            n_estimators=n_estimators,
            criterion=criterion,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_features=max_features,
            max_leaf_nodes=max_leaf_nodes,
            min_impurity_decrease=min_impurity_decrease,
            min_impurity_split=min_impurity_split,
            bootstrap=bootstrap,
            oob_score=oob_score,
            n_jobs=n_jobs,
            random_state=random_state,
            warm_start=warm_start,
            class_weight=class_weight)
        # mark if we can use it or not
        self._is_train = is_train
        self._input_size = 0

    @staticmethod
    def load(model: object):
        answer = RandomForestModel(is_train=True)
        answer._model = model
        return answer

    # ---> ml operation <--- #

    def train(self, x: list, y: list):
        """ train random forest model """
        self._model.fit(x, y)
        self._is_train = True
        self._input_size = len(x[0])

    def predict(self, x: list) -> list:
        """ given an input vector, find the right classification / regression """
        if self._is_train and isinstance(x, list) and (len(x) == self._input_size or len(x) == 0):
            return self._model.predict([x])[0]
        raise Exception("Cannot use untrained model")

    def predict_list(self, x: list) -> list:
        """ given an input of list of vectors, find the right classifications / regressions """
        if self._is_train and isinstance(x, list) and len(x) > 0 and isinstance(x[0], list) and (len(x) == self._input_size or len(x) == 0):
            return self._model.predict(x)

    def get_results(self, parms=[]) -> dict:
        """ get list of parms intresting to save, compare"""
        if len(parms) == 1:
            return {"acc": parms[0]}

    # ---> end - ml operation <--- #

    # ---> model versions <--- #

    @staticmethod
    def save_model_results(path: str, parms: dict) -> None:
        """ save a model results """
        # make sure we are good to go'
        # write it
        with open(path, 'w') as file:
            file.write(json.dumps(parms))

    @staticmethod
    def load_model_results(path: str):
        """ load a model results """
        # write it
        with open(path, 'r') as file:
            return json.load(file)

    @staticmethod
    def compare_model_results(path: str, parms: dict) -> bool:
        """ load model and compare to the database for the new one """
        return parms["acc"] >= RandomForestModel.load_model_results(path)["acc"]

    # ---> end - model versions <--- #

    # ---> getters <--- #

    # ---> end - getters <--- #

    # ---> model operation <--- #

    def save_file(self, file_path: str) -> None:
        """ get the parms of the model into file"""
        return ModelSaverLoader.save_model(path_to_file=file_path,
                                           model=pickle.dumps(self._model))

    @staticmethod
    def load_file(file_path: str) -> object:
        """ use parms to load model of LogisticRegression """
        return RandomForestModel.load(ModelSaverLoader.load_model(path_to_file=file_path))

    # ---> end - model operation <--- #
