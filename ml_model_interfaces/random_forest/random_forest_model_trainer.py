# library imports
from sklearn import metrics

# projects imports
from ml_model_interfaces.ml_model.ml_model_trainer import IMlModelTrainer
from ml_model_interfaces.random_forest.random_forest_model import RandomForestModel


class RandomForestModelTrainer(IMlModelTrainer):
    """ auto trainer to RandomForestModel model """

    def __init__(self,
                 n_estimators='warn',
                 criterion="gini",
                 max_depth=None,
                 min_samples_split=2,
                 min_samples_leaf=1,
                 min_weight_fraction_leaf=0.,
                 max_features="auto",
                 max_leaf_nodes=None,
                 min_impurity_decrease=0.,
                 min_impurity_split=None,
                 bootstrap=True,
                 oob_score=False,
                 n_jobs=None,
                 random_state=None,
                 warm_start=False,
                 class_weight=None):

        IMlModelTrainer.__init__(self)
        print("Start process")
        # build model
        self._model = RandomForestModel(
            n_estimators=n_estimators,
            criterion=criterion,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_features=max_features,
            max_leaf_nodes=max_leaf_nodes,
            min_impurity_decrease=min_impurity_decrease,
            min_impurity_split=min_impurity_split,
            bootstrap=bootstrap,
            oob_score=oob_score,
            n_jobs=n_jobs,
            random_state=random_state,
            warm_start=warm_start,
            class_weight=class_weight)

    def train_model(self,
                    save_model_results_path: str,
                    save_model_path: str,
                    x_train: list,
                    y_train: list,
                    x_test: list,
                    y_test: list,
                    save_anyway=False):
        """ open an random forest object, prepare database and train it. Test results, and save the better model """
        try:
            # train model
            self._model.train(x=x_train, y=y_train)
            # check performance
            print("Run on test")
            y_pred = self._model.predict_list(x=x_test)
            accuracy = metrics.accuracy_score(y_test, y_pred)
            print("Model ML Accuracy: {:.2f}".format(accuracy*100))
            if save_anyway or self._model.compare_model_results(path=save_model_results_path,
                                                                parms=self._model.get_results([accuracy])):
                # save model
                print("Save model to {}".format(save_model_path))
                self._model.save_file(file_path=save_model_path)
                # save model results
                print("Save model results to {}".format(save_model_results_path))
                self._model.save_model_results(path=save_model_results_path,
                                               parms=self._model.get_results([accuracy]))
            else:
                # tell that we won't save
                print("Did not save model results to {}, last model is better".format(save_model_results_path))
            print("End process")
        except Exception as error:
            print("Error at train_model from pre_classifier/model_trainer with sub error: " + str(error))

