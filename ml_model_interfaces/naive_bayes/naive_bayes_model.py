# library imports
import os
import json
import pickle
from sklearn.naive_bayes import GaussianNB

# projects imports
from ml_model_interfaces.ml_model.ml_model import IMlModel
from ml_model_interfaces.save_load_model import ModelSaverLoader


class NaiveBayesModel(IMlModel):
    """ naive bayes Model
        taking advantage of http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.46.1529 """

    def __init__(self,
                 var_smoothing=1e-10,
                 is_train=False):
        IMlModel.__init__(self)
        self._model = GaussianNB(var_smoothing=var_smoothing)
        self._is_train = is_train

    @staticmethod
    def load(model: object):
        answer = NaiveBayesModel(is_train=True)
        answer._model = model
        return answer

    # ---> ml operation <--- #

    def train(self, x: list, y: list):
        """ train random forest model """
        self._model.fit(x, y)
        self._is_train = True

    def predict(self, x: list) -> list:
        """ given an input vector, find the right classification / regression """
        if self._is_train and isinstance(x, list):
            return self._model.predict([x])[0]
        raise Exception("Cannot use untrained model")

    def predict_list(self, x: list) -> list:
        """ given an input of list of vectors, find the right classifications / regressions """
        if self._is_train and isinstance(x, list) and len(x) > 0 and isinstance(x[0], list):
            return self._model.predict(x)
        raise Exception("Cannot use untrained model")

    def get_results(self, parms=[]) -> dict:
        """ get list of parms intresting to save, compare"""
        if len(parms) == 1:
            return {"acc": parms[0]}
        raise Exception("Provide a single 'acc' parm")

    # ---> end - ml operation <--- #

    # ---> model versions <--- #

    @staticmethod
    def save_model_results(path: str, parms: dict) -> None:
        """ save a model results """
        # make sure we are good to go'
        if not (path is not None and os.path.isfile(path) and 0 <= parms["acc"] <= 100):
            raise Exception("arguments are not legit")
        # write it
        with open(path, 'w') as file:
            file.write(json.dumps(parms))

    @staticmethod
    def load_model_results(path: str):
        """ load a model results """
        # make sure we are good to go
        if not (path is not None and os.path.isfile(path)):
            raise Exception("path to file is not legit")
        # write it
        with open(path, 'r') as file:
            return json.load(file)

    @staticmethod
    def compare_model_results(path: str, parms: dict) -> bool:
        """ load model and compare to the database for the new one """
        # make sure we are good to go
        if not (path is not None and os.path.isfile(path)):
            raise Exception("arguments are not legit")
        # load all results
        old_results = NaiveBayesModel.load_model_results(path)
        # tell if better or not
        return True if parms["acc"] >= old_results["acc"] else False

    # ---> end - model versions <--- #

    # ---> getters <--- #

    # ---> end - getters <--- #

    # ---> model operation <--- #

    def save_file(self, file_path: str) -> None:
        """ get the parms of the model into file"""
        return ModelSaverLoader.save_model(path_to_file=file_path,
                                           model=pickle.dumps(self._model))

    @staticmethod
    def load_file(file_path: str) -> object:
        """ use parms to load model of LogisticRegression """
        return NaiveBayesModel.load(ModelSaverLoader.load_model(path_to_file=file_path))

    # ---> end - model operation <--- #
