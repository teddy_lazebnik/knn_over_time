# projects imports
from ml_model_interfaces.save_load_model import ModelSaverLoader


class IMlModel:
    """ Logistic Regression Model """

    def __init__(self):
        pass

    # ---> ml operation <--- #

    def train(self, x: list, y: list) -> None:
        """
        Train logistic regression instance
        :param x: the objects
        :param y: the labels
        :return: trained model
        """
        pass

    def predict(self, x: object) -> object:
        """ used the model once """
        pass

    def predict_list(self, x: list) -> list:
        """ used the model once """
        pass

    def get_results(self, parms=[]) -> dict:
        """ get list of parms intresting to save, compare"""
        pass

    # ---> end - ml operation <--- #

    # ---> model versions <--- #

    @staticmethod
    def save_model_results(path: str, parms: dict) -> None:
        """ save a model results """
        pass

    @staticmethod
    def load_model_results(path: str):
        """ load a model results """
        pass

    @staticmethod
    def compare_model_results(path: str, parms: dict) -> bool:
        """ load model and compare to the database for the new one """
        pass

    # ---> end - model versions <--- #

    # ---> model operation <--- #

    @staticmethod
    def save_file(file_path: str, model) -> None:
        """ get the parms of the model into file"""
        return ModelSaverLoader.save_model(path_to_file=file_path,
                                           model=model)

    @staticmethod
    def load_file(file_path: str) -> object:
        """ use parms to load model of LogisticRegression """
        return ModelSaverLoader.load_model(path_to_file=file_path)

    # ---> end - model operation <--- #


