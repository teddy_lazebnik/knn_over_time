# library imports
from sklearn import metrics

# projects imports
from ml_model_interfaces.ml_model.ml_model import IMlModel


class IMlModelTrainer:
    """ auto trainer to ML model """

    def __init__(self):
        pass

    @staticmethod
    def train_model(save_model_results_path: str,
                    save_model_path: str,
                    x_train: list,
                    y_train: list,
                    x_test: list,
                    y_test: list,
                    save_anyway=False):
        """ open an ml model object, prepare database and train it. Test results, show results and save model """
        try:
            print("Start process")
            # build model
            model = IMlModel()
            # train model
            model.train(x=x_train, y=y_train)
            # check performance
            print("Run on test")
            y_pred = model.predict_list(x=x_test)
            accuracy = metrics.accuracy_score(y_test, y_pred)
            print("Model ML Accuracy: {:.2f}".format(accuracy*100))
            if save_anyway or model.compare_model_results(path=save_model_results_path,
                                                          parms=model.get_results()):
                # save model
                print("Save model to {}".format(save_model_path))
                model.save_file(file_path=save_model_path, model=model)
                # save model results
                print("Save model results to {}".format(save_model_results_path))
                model.save_model_results(path=save_model_results_path,
                                         parms=model.get_results([]))
            else:
                # tell that we won't save
                print("Did not save model results to {}, last model is better".format(save_model_results_path))
            print("End process")
        except Exception as error:
            print("Error at train_model from pre_classifier/model_trainer with sub error: " + str(error))
