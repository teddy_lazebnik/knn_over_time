# library imports 
import os
import pickle


class ModelSaverLoader:
    """ Allow to save and load ML models to\from local file """

    def __init__(self):
        pass

    @staticmethod
    def save_model(path_to_file: str, model: object) -> None:
        """ save a model to a path """
        if path_to_file is not None and os.path.isfile(path_to_file) and model is not None:
            with open(path_to_file, "wb") as model_file:
                model_file.write(pickle.dumps(model))

    @staticmethod
    def load_model(path_to_file: str) -> object:
        """ load a model from a path """
        if path_to_file is not None and os.path.isfile(path_to_file):
            with open(path_to_file, "rb") as model_file:
                return pickle.loads(model_file.read())

    @staticmethod
    def save_model_naive(path_to_file: str, model: object) -> None:
        with open(path_to_file, "wb") as model_file:
            pickle.dump(model, model_file)

    @staticmethod
    def load_model_naive(path_to_file: str) -> object:
        with open(path_to_file, "rb") as model_file:
            return pickle.load(model_file)
