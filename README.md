## Knn & Kmeans extensions

**An extension project of the classic Knn algorithm**

This repository provides three different extensions for the [K-means](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html) and [Knn](https://scikit-learn.org/stable/modules/neighbors.html) algorithm provided in the "sklearn" module.

1. **X-means** which finds the best 'k' hyper-parameter in K-means automatically for given possible set of K's
2. **Knn-regression** convert the classic Knn classification problem to a regression predictor.
3. **Knn over time** allowing to find casters using the Knn algorithm with a strong time component. This technique inherently using changing of clusters over time and not just another dimension in the feature-space.

---

## Installation

One either include the two needed folders of this repo in the project or install the repo initially as module.

The **math_ops** has classical mathematical concepts and operation in an explicit class format. 
They may be used to benifit from more C# like programing, but there porpose is to mainly support the **ml_model_interfaces**.

The **ml_model_interfaces** has the three implementations of the extensions explained above.

---

## Usage Example

Assume we wish to train a **knn over time** model for the following data:

```python
x = [[[2], [2], [2]], [[1], [1], [1]], [[1], [1], [1]], [[-1], [0], [1]], [[5], [5], [5]]]
y = [[2, 2, 2], [1, 1, 1], [3, 3, 3], [-2, 0, 2], [5, 5, 5]]
```

as points in 1-dim changing over time (x) time and there value over time (y), respectably.

One can do as following:

```python
from ml_model_interfaces.knn_over_time.knn_over_time_model import KnnTimeModel

# we need to pick how many clasters do we want and how we fit the change over time
model = KnnTimeModel(k=3, poly_degree=1)
model.train(x=x, y=y)
# that it, we can ask the models answer for a given point, in a given time
prediction = model.predict(x=[2], time=3)
```

