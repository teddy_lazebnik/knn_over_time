"""
Hold all the distances function we wish to declare in one place
"""
import math


class Distances:
    """ Hold all the distances function we wish to declare in one place """

    def __init__(self):
        pass

    @staticmethod
    def l1(p1: list, p2: list) -> float:
        if len(p1) == len(p2) and len(p1) > 0:
            return sum([math.fabs(p1[index] - p2[index]) for index in range(len(p1))])
        raise Exception("vector size must be same and bigger then 0")

    @staticmethod
    def l2(p1: list, p2: list) -> float:
        return Distances.lp(point1=p1, point2=p2, p=2)

    @staticmethod
    def lp(point1: list, point2: list, p: int) -> float:
        if p >= 1 and len(point1) == len(point2) and len(point1) > 0:
            return math.pow(sum([math.pow(point1[index] - point2[index], p) for index in range(len(point1))]), 1/p)
        raise Exception("Cannot work with p less then 1 and not int")

    @staticmethod
    def l_inf(point1: list, point2: list) -> float:
        if len(point1) == len(point2) and len(point1) > 0:
            return max([math.fabs(point1[index] - point2[index]) for index in range(len(point1))])
        raise Exception("Cannot work with p less then 1 and not int")

    @staticmethod
    def norm(vector: list, p=2) -> float:
        """ calc the Lp norm of an vector """
        if isinstance(vector, list) and len(vector) > 0 and p >=1:
            return math.pow(sum([math.pow(math.fabs(value), p) for value in vector]), 1/p)
        raise Exception("Cannot work with p less then 1 and not int")
