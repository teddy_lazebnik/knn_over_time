# library imports
from scipy.stats import chisquare, shapiro, normaltest

# project imports
from math_ops.statistical import Statistical


class DistributionClassier:
    """ check if a desctbution is one of a well known distributions or classify automaticly """

    def __init__(self):
        pass

    @staticmethod
    def classify(signal_over_time: list,
                 alpha_confidance=0.05) -> tuple:
        """ global function which is just a switch - case for all the distributions we know """
        if DistributionClassier.is_uniform(signal_over_time=signal_over_time,
                                           alpha_confidance=alpha_confidance):
            return "uniform", {"min": min(signal_over_time), "max": max(signal_over_time)}
        elif DistributionClassier.is_normal(signal_over_time=signal_over_time,
                                            alpha_confidance=alpha_confidance):
            return "normal", {"mean": Statistical.mean(signal_over_time), "std": Statistical.std(signal_over_time)}
        else:
            return "unknown", {}

    @staticmethod
    def is_uniform(signal_over_time: list,
                   alpha_confidance=0.05) -> bool:
        """
        check if a signal over time is uniform
        take use of: https://web.archive.org/web/20171022032306/http://vassarstats.net:80/textbook/ch8pt1.html
        :param signal_over_time: signal over time
        :param alpha_confidance: the alpha value which p should be better from
        :return: if uniform distribution or not
        """
        return chisquare(signal_over_time)[1] >= (1-alpha_confidance)

    @staticmethod
    def is_normal(signal_over_time: list,
                  alpha_confidance=0.05,
                  full_agreement=False) -> bool:
        """
        check if a signal over time is normal
        :param signal_over_time: signal over time
        :param alpha_confidance: the alpha value which p should be better from
        :param full_agreement: if both methods agree this is normal or at least one of them agree
        :return: if normal distribution or not
        """
        d_agostino_test = normaltest(signal_over_time)[1] >= (1-alpha_confidance)
        shapiro_test = shapiro(signal_over_time)[1] >= (1-alpha_confidance)
        return (full_agreement and d_agostino_test and shapiro_test) \
               or (not full_agreement and (d_agostino_test or shapiro_test))
