"""
This file is an abstract class with operation on vectors
"""
from math_ops.distances import Distances


class VectorsOperations:
    """ Allow to make operations on vectors """

    def __init__(self):
        pass

    @staticmethod
    def abs(vector: list) -> list:
        """ abs each item in the vector """
        if len(vector) > 0:
            return [abs(value) for value in vector]
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def gradient(vector: list, append=False) -> list:
        """ abs each item in the vector """
        if isinstance(vector, list) and len(vector) > 0:
            if append:
                answer = [0.0]
            else:
                answer = []
            answer.extend([(vector[index + 1] - vector[index - 1]) / 2
                           for index in range(1, len(vector) - 1)])
            if append:
                answer.append(0.0)
            return answer
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def div(vector: list, n=1, append=False) -> list:
        """ abs each item in the vector """
        if n == 1:
            return VectorsOperations.gradient(vector=vector, append=append)
        elif 1 < n < len(vector) - 1:
            return VectorsOperations.div(vector=VectorsOperations.gradient(vector=vector, append=append), n=n-1)
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def scalar(vector: list, factor: float) -> list:
        """ mult vector with scalar """
        if len(vector) > 0:
            return [value * factor for value in vector]
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def aggregate(vector: list, step_size: int) -> list:
        """ local sums in the array, ignore_tail says if we wish to clear the tail if needed"""
        if (len(vector) > step_size or len(vector) == 0) and isinstance(step_size, int) and step_size > 0:
            return [sum(vector[i:i+step_size]) for i in range(0, len(vector), step_size)]
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def sum(p1: list, p2: list) -> list:
        """ sum 2 vectors """
        if len(p1) == len(p2) and len(p1) > 0:
            return [p1[index] + p2[index] for index in range(len(p1))]
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def sub(p1: list, p2: list) -> list:
        """ sub 2 vectors """
        if len(p1) == len(p2) and len(p1) > 0:
            return [p1[index] - p2[index] for index in range(len(p1))]
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def mult(p1: list, p2: list) -> list:
        """ mult 2 vectors """
        if len(p1) == len(p2) and len(p1) > 0:
            return [p1[index] * p2[index] for index in range(len(p1))]
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def divide(p1: list, p2: list) -> list:
        """ divide 2 vectors """
        if len(p1) == len(p2) and len(p1) > 0:
            return [p1[index] / p2[index] for index in range(len(p1))]
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def producte(p1: list, p2: list) -> float:
        """ dot producte 2 vectors """
        if len(p1) == len(p2) and len(p1) > 0:
            return sum([p1[index] * p2[index] for index in range(len(p1))])
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def is_orthogonal(p1: list, p2: list) -> bool:
        """ check if 2 vectors are orthogonal """
        if len(p1) == len(p2) and len(p1) > 0:
            return VectorsOperations.producte(p1, p2) == 0
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def norm_vector(vector: list, p=2) -> list:
        """ give a normed vector for the given one """
        if len(vector) > 0:
            norm = Distances.norm(vector=vector, p=p)
            if norm != 0:
                return VectorsOperations.scalar(vector=vector, factor=1/norm)
            else:
                # zero index vector from the same size
                return [0 for index in range(len(vector))]
        raise Exception("Vector size must be same and bigger then 0")

    @staticmethod
    def cosine_similarity(p1: list, p2: list) -> float:
        """ calc the cosine similarity between 2 vectors """
        if len(p1) == len(p2) and len(p1) > 0:
            return VectorsOperations.producte(p1, p2) / (Distances.norm(p1) * Distances.norm(p2))
        raise Exception("Vector size must be same and bigger then 0")

