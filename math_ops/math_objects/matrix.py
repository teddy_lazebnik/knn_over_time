# library imports
import os
import math
import numpy
import seaborn as sns
import matplotlib.pyplot as plt


class Matrix:
    """ A mathematical matrix object """

    def __init__(self, dim1: int, dim2: int, data=None):
        if dim1 >= 1 and dim2 >= 1:
            self._dim1 = dim1
            self._dim2 = dim2
        else:
            raise Exception("Dim of the matrix must be integer bigger then 1")

        # try to assign data
        if not self.assign(matrix_data=data):
            # defaults data
            self._data = self._black_matrix(dim1=self._dim1, dim2=self._dim2)

    @staticmethod
    def init(data: list):
        """ hard init matrix object from raw data as a 2d-array"""
        # build object
        answer = Matrix(dim1=len(data),
                        dim2=len(data[0]))
        # set data
        answer.assign(matrix_data=data)
        return answer

    @staticmethod
    def from_csv(csv_path: str, item_spliter=",", row_spliter="\n"):
        """ hard init matrix object from a csv file"""
        if not (isinstance(csv_path, str) and len(csv_path) > 0 and
                os.path.isfile(csv_path) and csv_path.endswith(".csv")):
            raise Exception("Cannot find the csv file to load matrix from")
        try:
            with open(csv_path, "r") as matrix_file:
                text_data = matrix_file.read()
            lines = text_data.split(row_spliter)
            data = [line.split(item_spliter) for line in lines]
            data = [[float(data[i][j]) for j in range(len(data[i]))] for i in range(len(data))]
            # build object
            answer = Matrix(dim1=len(data),
                            dim2=len(data[0]))
            # set data
            answer.assign(matrix_data=data)
            return answer
        except Exception as error:
            raise Exception("Error while processing, saying: {}".format(error))

    # ---> operations <--- #

    def same_shape(self, matrix) -> bool:
        """ check if the matrix is the same shape as this one """
        return True if self._dim1 == matrix.dim_1() and self._dim2 == matrix.dim_2() else False

    def binaryish(self, threshold=None):
        """ convert the matrix to binary one according to fitting trashold or fixed one """
        if threshold is None:
            threshold = self.avg()
        return self.to_matrix([[0 if self._data[i][j] < threshold else 1
                                for j in range(self._dim2)]
                               for i in range(self._dim1)])

    def scalar(self, scalar: float):
        """ mult by scalar the matrix """
        if scalar == 0:
            return self._black_matrix(dim1=self._dim1, dim2=self._dim2)
        else:
            return self.to_matrix([[self._data[i][j] * scalar
                                    for j in range(self._dim2)]
                                   for i in range(self._dim1)])

    def add(self, matrix):
        """ add the other matrix with this one """
        # check if we are fine to operate
        if not self.same_shape(matrix=matrix):
            raise Exception("Matrix shape must agree")
        return self.to_matrix([[self._data[i][j] + matrix.get(i, j)
                                for j in range(self._dim2)]
                               for i in range(self._dim1)])

    def sub(self, matrix):
        """ sub the other matrix with this one """
        # check if we are fine to operate
        if not self.same_shape(matrix=matrix):
            raise Exception("Matrix shape must agree")
        return self.to_matrix([[self._data[i][j] - matrix.get(i, j)
                                for j in range(self._dim2)]
                               for i in range(self._dim1)])

    def mult(self, matrix, using_numpy=True):
        """ mult the 2 matrix """
        # check if we are fine to operate
        if not (self._dim2 == matrix.dim_1()):
            raise Exception("Matrix dims must agree")
        # run matrix mult using numpy's algorithm
        if using_numpy:
            return self.to_matrix(numpy.matmul(numpy.array(self._data), numpy.array(matrix._data)).tolist())
        # run classic multilation without numpy
        result = self._black_matrix(dim1=self._dim1, dim2=matrix.dim_2())
        [[[self._add_value_to_matrix_data(result, i, j, self._data[i][k] * matrix.get(k, j))
           for k in range(matrix.dim_1())]
          for j in range(matrix.dim_2())]
         for i in range(self._dim1)]
        return self.to_matrix(result)

    def transpose(self):
        """ transpose this matrix """
        return self.to_matrix([list(i) for i in zip(*self._data)])

    def tr(self):
        """ return the trace of squre matrix """
        # check if we are fine to operate
        if not (self.is_squre()):
            raise Exception("Matrix must be squre")
        return sum([self._data[i][i] for i in range(self._dim1)])

    def inner(self, matrix):
        """ mult the 2 matrix """
        return self.mult(matrix=matrix).tr()

    def scale_coloums(self, col_to_scale: int, ignore_tail=False):
        """ try to scale the matrix according to the coloums, each 'col_to_scale' will be added to a single value """
        if not (isinstance(col_to_scale, int) and col_to_scale > 0):
            raise Exception("col_to_scale must be a positive integer")
        if not ignore_tail and (self.dim_1() % col_to_scale) != 0 or col_to_scale == 0:
            raise Exception("cannot equally divide {} with {}".format(self.dim_2(), col_to_scale))
        new_size = int(self.dim_2() / col_to_scale)
        answer = self._black_matrix(self.dim_1(), new_size)
        [[[self._add_value_to_matrix_data(answer, j, i, self._data[j][k])
           for k in range(i * col_to_scale, (i + 1) * col_to_scale)]
          for j in range(self._dim1)]
         for i in range(new_size)]
        return self.to_matrix(answer)

    def scale_rows(self, rows_to_scale: int, ignore_tail=False):
        """ try to scale the matrix according to the rows, each 'rows_to_scale' will be added to a single value """
        return self\
            .transpose()\
            .scale_coloums(col_to_scale=rows_to_scale,
                           ignore_tail=ignore_tail)\
            .transpose()

    def norm(self, p=2) -> float:
        """ norm of the matrix """
        if type(p) not in [int, str] or \
                (isinstance(p, int) and not (p >= 1 or p in [-1, 0])) or \
                (isinstance(p, str) and p.strip().lower() not in ['sum', 'inf', 'max']):
            raise Exception("Norm P must be integer bigger or equal then 1 or 'sum', 'inf', 'max'")
        if isinstance(p, int) and p == 1:
            return max([sum([abs(self._data[i][j])
                             for j in range(self._dim2)])
                        for i in range(self._dim1)])
        elif (isinstance(p, int) and p == 0) or (isinstance(p, str) and p.strip().lower() == "inf"):
            return max([sum([abs(self._data[i][j])
                             for i in range(self._dim1)])
                        for j in range(self._dim2)])
        elif (isinstance(p, int) and p == -1) or (isinstance(p, str) and p.strip().lower() == "max"):
            return max([max([abs(self._data[i][j])
                             for i in range(self._dim1)])
                        for j in range(self._dim2)])
        elif isinstance(p, str) and p.strip().lower() == "sum":
            return sum([sum([self._data[i][j]
                             for j in range(self._dim2)])
                        for i in range(self._dim1)])
        else:
            return math.pow(sum([sum([math.pow(self._data[i][j], p)
                                      for j in range(self._dim2)])
                                 for i in range(self._dim1)]), 1/p)

    def _add_value_to_matrix_data(self, result: list, i: int, j: int, value):
        """ help function for the mult function """
        result[i][j] += value

    def is_diagonal(self):
        """ check if diagonal matrix """
        for i in range(self._dim1):
            for j in range(self._dim2):
                if i != j and self._data[i][j] != 0:
                    return False
        return True

    def is_trigonal(self):
        """ check if trigonal matrix """
        for i in range(self._dim1 - 1):
            for j in range(i + 1, self._dim2):
                if self._data[j][i] != 0:
                    return False
        return True

    def is_jordan(self):
        """ check if jordan matrix """
        for i in range(self._dim1):
            for j in range(self._dim2):
                if i != j and j != (i + 1) and self._data[i][j] != 0:
                    return False
        return True

    def is_symmetric(self):
        """ check if symmetric matrix """
        for i in range(self._dim1):
            for j in range(self._dim2):
                if self._data[i][j] != self._data[j][i]:
                    return False
        return True

    # ---> end - operations <--- #

    # ---> meta functions <--- #

    def get(self, i: int, j: int) -> float:
        return self._data[i][j]

    def set(self, i: int, j: int, value: float) -> None:
        self._data[i][j] = value

    def assign(self, matrix_data: list, hard=False) -> bool:
        """ try to update the data of the matrix """
        if hard or (isinstance(matrix_data, list) and
                    len(matrix_data) == self._dim1 and
                    isinstance(matrix_data[0], list) and
                    len(matrix_data[0]) == self._dim2):
            self._data = matrix_data
            if hard:
                self._dim1 = len(matrix_data)
                self._dim2 = len(matrix_data[0])
            return True
        return False

    @staticmethod
    def to_matrix(matrix_data: list):
        """ convert data to Matrix object """
        return Matrix.init(data=matrix_data)

    def shape(self) -> tuple:
        return self._dim1, self._dim2

    def dim_1(self) -> int:
        return self._dim1

    def dim_2(self) -> int:
        return self._dim2

    def size(self) -> int:
        return self._dim1 * self._dim2

    def is_squre(self) -> int:
        return self._dim1 == self._dim2

    @staticmethod
    def _black_matrix(dim1: int, dim2: int) -> list:
        return [[0 for j in range(dim2)] for i in range(dim1)]

    @staticmethod
    def eye_matrix(dim: int) -> list:
        return [[1 if i == j else 0 for j in range(dim)] for i in range(dim)]

    @staticmethod
    def make_symmetric(matrix):
        """
        make an inputted matrix as symmetric
        :param matrix: inputed matrix
        :return: same matrix but symmetric
        """
        # make sure we can use the input
        return Matrix.to_matrix([[matrix._data[i][j] if i <= j else matrix._data[j][i]
                                  for j in range(matrix.dim_2())]
                                 for i in range(matrix.dim_1())])

    def __repr__(self) -> str:
        return "<Matrix: (rows={} | cols={})>".format(self._dim1, self._dim2)

    def __str__(self) -> str:
        answer = "["
        for i in range(self._dim1):
            answer += "["
            for j in range(self._dim2):
                answer += "{}, ".format(self._data[i][j])
            answer = answer[:-2] + "], "
        answer = answer[:-2] + "]"
        return answer

    # ---> end - meta functions <--- #

    # ---> meta functions <--- #

    def to_csv(self, path: str, item_spliter=",", row_spliter="\n") -> None:
        """ convert the matrix into csv file """
        with open(path, "w") as answer_file:
            for row in self._data:
                for col in row[:-1]:
                    answer_file.write("{}{}".format(col, item_spliter))
                answer_file.write("{}{}".format(row[-1], row_spliter))

    def avg(self) -> float:
        """ get the avg(avg(matrix)) value """
        return sum([sum([self._data[i][j] for j in range(self._dim2)]) for i in range(self._dim1)]) / self.size()

    def show_as_heatmap(self,
                        figsize=(10, 20),
                        show_values=True,
                        linewidth=0.5,
                        vmax=250,
                        vmin=-250,
                        cmap="Greys",
                        title=None,
                        square=False) -> None:
        """ convert the matrix into csv file """
        # set size
        plt.figure(figsize=figsize)
        # build map
        ax = sns.heatmap(numpy.asarray(self._data),
                         linewidth=linewidth,
                         vmax=vmax,
                         vmin=vmin,
                         center=0,
                         cmap=cmap,
                         annot=show_values,
                         square=square)
        # put values
        # write title
        if isinstance(title, str) and len(title) > 0:
            ax.set_title(title)

        plt.show()

    # ---> end - meta functions <--- #

    # ---> analysis functions <--- #

    def sparse(self, epsilon_machine=0.0001) -> float:
        """ calc how much sparse a matrix is """
        return sum([sum([1 if abs(self._data[i][j]) < epsilon_machine else 0
                         for i in range(self._dim1)])
                    for j in range(self._dim2)]) / self.size()

    # ---> end - analysis functions <--- #
