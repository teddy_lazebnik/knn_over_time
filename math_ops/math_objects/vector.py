# library imports

# project imports
from math_ops.vector_operation import VectorsOperations


class Vector:
    """ A mathematical vector object """

    def __init__(self, dim: int, data=None):
        if dim >= 1:
            self._dim = dim
        else:
            raise Exception("Dim of the matrix must be integer bigger then 1")

        # try to assign data
        if not self.assign(vector_data=data):
            # defaults data
            self._data = self._black_vector(dim=dim)

    @staticmethod
    def init(data: list):
        """ hard init Vector object from raw data as a 1d-array"""
        # build object
        answer = Vector(dim=len(data))
        # set data
        answer.assign(vector_data=data)
        return answer

    # ---> operations <--- #

    def cut(self, new_dim: int):
        """ extend this vector or cut """
        if new_dim > self._dim:
            answer = self._data.copy()
            answer.extend([0 for index in range(self._dim - new_dim)])
            return answer
        return Vector.to_vector(self._data[:new_dim].copy())

    def same_shape(self, vector) -> bool:
        """ check if the vector is the same shape as this one """
        return True if self._dim == vector.dim() else False

    def scalar(self, scalar: float):
        """ mult by scalar the matrix """
        if scalar == 0:
            return self._black_vector(dim=self._dim)
        else:
            return Vector.to_vector(VectorsOperations.scalar(self._data, scalar))

    def aggregate(self, step_size: int):
        """ local sums in the array, ignore_tail says if we wish to clear the tail if needed"""
        return Vector.to_vector(VectorsOperations.aggregate(self._data, step_size))

    def div(self, n: int = 1, append: bool = True):
        """ local sums in the array, ignore_tail says if we wish to clear the tail if needed"""
        return Vector.to_vector(VectorsOperations.div(self._data, n=n, append=append))

    def add(self, vector):
        """ add the other vector with this one """
        return Vector.to_vector(VectorsOperations.sum(self._data, vector))

    def sub(self, vector):
        """ sub the other vector with this one """
        return Vector.to_vector(VectorsOperations.sub(self._data, vector))

    def mult(self, vector):
        """ mult the 2 vector """
        return Vector.to_vector(VectorsOperations.mult(self._data, vector))

    def divide(self, vector):
        """ divide 2 vectors """
        return Vector.to_vector(VectorsOperations.divide(self._data, vector))

    def producte(self, vector) -> float:
        """ dot producte 2 vectors """
        return VectorsOperations.producte(self._data, vector)

    def is_orthogonal(self, vector) -> bool:
        """ check if 2 vectors are orthogonal """
        return VectorsOperations.is_orthogonal(self._data, vector)

    def cosine_similarity(self, vector) -> float:
        """ calc the cosine similarity between 2 vectors """
        return VectorsOperations.cosine_similarity(self._data, vector)

    def norm(self, p=2):
        """ norm of the vector """
        return Vector.to_vector(VectorsOperations.norm_vector(vector=self._data, p=p))

    # ---> end - operations <--- #

    # ---> meta functions <--- #

    def get(self, index: int) -> float:
        return self._data[index]

    def set(self, index: int, value: float) -> None:
        self._data[index] = value

    def assign(self, vector_data: list, hard=False) -> bool:
        """ try to update the data of the vector """
        if hard or (isinstance(vector_data, list) and len(vector_data) == self._dim):
            self._data = vector_data
            if hard:
                self._dim = len(vector_data)
            return True
        return False

    @staticmethod
    def to_vector(vector_data: list):
        """ convert data to Vector object """
        return Vector.init(data=vector_data)

    def to_list(self) -> list:
        """ convert data to Vector object """
        return self._data

    def shape(self) -> tuple:
        return self._dim, 1

    def dim(self) -> int:
        return self._dim

    def copy(self):
        answer = Vector(dim=self._dim)
        [answer.set(index=index,
                    value=value)
         for index, value in enumerate(self._data)]
        return answer

    def sigma(self) -> float:
        return sum([item for item in self._data])

    def pie(self) -> float:
        answer = 1
        for item in self._data:
            answer *= item
        return answer

    @staticmethod
    def _black_vector(dim: int) -> list:
        return [0 for i in range(dim)]

    def __repr__(self) -> str:
        return "<Vector: ({})>".format(self._dim)

    def __str__(self) -> str:
        return self._data.__str__()

    # ---> end - meta functions <--- #

    # ---> meta functions <--- #

    def to_csv(self, path: str, item_spliter=",") -> None:
        """ convert the matrix into csv file """
        with open(path, "w") as answer_file:
            answer_file.write("[")
            last_index = len(self._data) - 2
            for index, item in enumerate(self._data):
                if index != last_index:
                    answer_file.write("{}{}".format(item, item_spliter))
                else:
                    answer_file.write("{}".format(item))
            answer_file.write("]")

    # ---> end - meta functions <--- #
