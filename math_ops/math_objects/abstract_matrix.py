# library imports

# our imports
from math_ops.math_objects.matrix import Matrix
from math_ops.math_objects.sparse_matrix import SparseMatrix


class AbstractMatrix:
    """
    an overview matrix class implements a super logic upon
    sparse and normal matrix representation
    """

    # enums #
    normal = 0
    sparse = 1
    # end - enums #

    # consts #
    memory_per_point_sparse = 4
    # end - consts #

    def __init__(self, abstract_matrix):
        # make sure we can work
        if type(abstract_matrix) in [Matrix, SparseMatrix]:
            # find which representation in better #
            # if normal
            if AbstractMatrix.best_representation(abstract_matrix=abstract_matrix) == AbstractMatrix.normal:
                self._normal = abstract_matrix if isinstance(abstract_matrix, Matrix) else abstract_matrix.to_matrix()
                self._sparse = None
            # if sparse
            else:
                self._normal = None
                self._sparse = abstract_matrix if isinstance(abstract_matrix, SparseMatrix) else  SparseMatrix.from_matrix(matrix=abstract_matrix)
        # if we can't just let the caller to know
        else:
            raise Exception("Argument can be Matrix or SparseMatrix object")

    def __repr__(self) -> str:
        # case 1 -> (normal)
        if self._normal is not None:
            return "<AbstractMatrix {}>".format(self._normal.__repr__())
        # case 2 -> (sparse, sparse)
        elif self._sparse is not None:
            return "<AbstractMatrix {}>".format(self._sparse.__repr__())
        else:
            return "<AbstractMatrix - empty>"

    def __str__(self) -> str:
        # case 1 -> (normal)
        if self._normal is not None:
            return self._normal.__str__()
        # case 2 -> (sparse, sparse)
        elif self._sparse is not None:
            return self._sparse.__str__()
        else:
            return "<AbstractMatrix - empty>"

    @staticmethod
    def init_matrix(data: list):
        """ init abstract matrix from matrix """
        return AbstractMatrix(Matrix.init(data=data))

    @staticmethod
    def init_sparse_matrix(data: list):
        """ init abstract matrix from sparse matrix """
        return AbstractMatrix(SparseMatrix.init(data=data))

    @staticmethod
    def best_representation(abstract_matrix) -> int:
        """
        saying for a given sparse / normal matrix the best representation for it
        Complexity:
            Worst case: O(n^2) when Matrix with (nxn) cause we need to run over all the matrix
            best case: O(1) when SparseMatrix with (n - points) cause we need to do 3 operations (len, mult, divide)
        :param abstract_matrix:
        :return:
        """
        # if sparse check if we wish to keep or change
        if type(abstract_matrix) in [Matrix, SparseMatrix]:
            return AbstractMatrix.sparse if abstract_matrix.sparse() > 1 / AbstractMatrix.memory_per_point_sparse else AbstractMatrix.normal
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def get_normal_representation(self) -> Matrix:
        # case 1 -> (normal)
        if self._normal is not None:
            return self._normal
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return self._sparse.to_matrix()
        # not any case, throw error
        raise Exception("Strange error, we does not have both representation matrices")

    def get_sparse_representation(self) -> SparseMatrix:
        # case 1 -> (normal)
        if self._normal is not None:
            return SparseMatrix.from_matrix(matrix=self._normal)
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return self._spars
        # not any case, throw error
        raise Exception("Strange error, we does not have both representation matrices")

    # ---> operations <--- #

    def same_shape(self, matrix) -> bool:
        # case 1 -> (normal, normal)
        if self._normal is not None and isinstance(matrix, Matrix):
            return self._normal.same_shape(matrix=matrix)
        # case 2 -> (sparse, sparse)
        elif self._sparse is not None and isinstance(matrix, SparseMatrix):
            return self._sparse.same_shape(matrix=matrix)
        # case 3 -> (normal, sparse)
        elif self._normal is not None and isinstance(matrix, SparseMatrix):
            return self._normal.dim_1() == matrix.dim_1() and self._normal.dim_2() == matrix.dim_2()
        # case 4 -> (sparse, normal)
        elif self._sparse is not None and isinstance(matrix, Matrix):
            return self._normal.dim_1() == matrix.dim_1() and self._normal.dim_2() == matrix.dim_2()
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def scalar(self, scalar):
        # case 1 -> (normal)
        if self._normal is not None:
            return AbstractMatrix(self._normal.scalar(scalar=scalar))
        # case 2 -> (sparse, sparse)
        elif self._sparse is not None:
            return AbstractMatrix(self._sparse.scalar(scalar=scalar))
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def add(self, matrix):
        # case 1 -> (normal, normal)
        if self._normal is not None and isinstance(matrix, Matrix):
            return AbstractMatrix(self._normal.add(matrix=matrix))
        # case 2 -> (sparse, sparse)
        elif self._sparse is not None and isinstance(matrix, SparseMatrix):
            return AbstractMatrix(self._sparse.add(matrix=matrix))
        # case 3 -> (normal, sparse)
        elif self._normal is not None and isinstance(matrix, SparseMatrix):
            if not self.same_shape(matrix):
                raise Exception("Matrix shape must agree")
            new_matrix = self._normal
            for point in matrix._data:
                new_matrix._data[point.i][point.j] += point.value
            return AbstractMatrix(new_matrix)
        # case 4 -> (sparse, normal)
        elif self._sparse is not None and isinstance(matrix, Matrix):
            if not self.same_shape(matrix):
                raise Exception("Matrix shape must agree")
            new_matrix = matrix
            for point in self._sparse._data:
                new_matrix._data[point.i][point.j] += point.value
            return AbstractMatrix(new_matrix)
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def sub(self, matrix):
        # case 1 -> (normal, normal)
        if self._normal is not None and isinstance(matrix, Matrix):
            return AbstractMatrix(self._normal.sub(matrix=matrix))
        # case 2 -> (sparse, sparse)
        elif self._sparse is not None and isinstance(matrix, SparseMatrix):
            return AbstractMatrix(self._sparse.sub(matrix=matrix))
        # case 3 -> (normal, sparse)
        elif self._normal is not None and isinstance(matrix, SparseMatrix):
            if not self.same_shape(matrix):
                raise Exception("Matrix shape must agree")
            new_matrix = self._normal
            for point in matrix._data:
                new_matrix._data[point.i][point.j] -= point.value
            return AbstractMatrix(new_matrix)
        # case 4 -> (sparse, normal)
        elif self._sparse is not None and isinstance(matrix, Matrix):
            if not self.same_shape(matrix):
                raise Exception("Matrix shape must agree")
            new_matrix = matrix
            for point in self._sparse._data:
                new_matrix._data[point.i][point.j] -= point.value
            return AbstractMatrix(new_matrix)
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def mult(self, matrix):
        # case 1 -> (normal, normal)
        if self._normal is not None and isinstance(matrix, Matrix):
            return AbstractMatrix(self._normal.mult(matrix=matrix))
        # case 2 -> (sparse, sparse)
        elif self._sparse is not None and isinstance(matrix, SparseMatrix):
            return AbstractMatrix(self._sparse.mult(matrix=matrix))
        # case 3 -> (normal, sparse)
        elif self._normal is not None and isinstance(matrix, SparseMatrix):
            return AbstractMatrix(self._normal.mult(matrix.to_matrix()))
        # case 4 -> (sparse, normal)
        elif self._sparse is not None and isinstance(matrix, Matrix):
            return AbstractMatrix(matrix.mult(self.sparse.to_matrix()))
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def transpose(self):
        # case 1 -> (normal)
        if self._normal is not None:
            return AbstractMatrix(self._normal.transpose())
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return AbstractMatrix(self._sparse.transpose())
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def tr(self) -> float:
        # case 1 -> (normal)
        if self._normal is not None:
            return self._normal.tr()
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return self._sparse.tr()
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def norm(self, p=2) -> float:
        # case 1 -> (normal)
        if self._normal is not None:
            return self._normal.norm(p)
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return self._sparse.norm(p)
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def is_diagonal(self) -> bool:
        # case 1 -> (normal)
        if self._normal is not None:
            return self._normal.is_diagonal()
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return self._sparse.is_diagonal()
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def is_trigonal(self) -> bool:
        # case 1 -> (normal)
        if self._normal is not None:
            return self._normal.is_trigonal()
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return self._sparse.is_trigonal()
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def is_jordan(self) -> bool:
        # case 1 -> (normal)
        if self._normal is not None:
            return self._normal.is_jordan()
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return self._sparse.is_jordan()
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def get(self, i: int, j: int) -> float:
        # case 1 -> (normal)
        if self._normal is not None:
            return self._normal.get(i, j)
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return self._sparse.get(i, j)
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def sparse(self) -> float:
        # case 1 -> (normal)
        if self._normal is not None:
            return self._normal.sparse()
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return self._sparse.sparse()
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def size(self) -> float:
        # case 1 -> (normal)
        if self._normal is not None:
            return self._normal.size()
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return self._sparse.size()
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def scale_coloums(self, col_to_scale=30, ignore_tail=True):
        # case 1 -> (normal)
        if self._normal is not None:
            return AbstractMatrix(self._normal.scale_coloums(col_to_scale=col_to_scale, ignore_tail=ignore_tail))
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return AbstractMatrix(self._sparse.to_matrix().scale_coloums(col_to_scale=col_to_scale, ignore_tail=ignore_tail))
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def scale_rows(self, rows_to_scale: int, ignore_tail=False):
        # case 1 -> (normal)
        if self._normal is not None:
            return AbstractMatrix(self._normal.scale_coloums(rows_to_scale=rows_to_scale, ignore_tail=ignore_tail))
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return AbstractMatrix(self._sparse.to_matrix().scale_coloums(rows_to_scale=rows_to_scale, ignore_tail=ignore_tail))
        # not any case, throw error
        raise Exception("Argument can be Matrix or SparseMatrix object")

    def avg(self) -> float:
        """ get the avg(avg(matrix)) value """
        # case 1 -> (normal)
        if self._normal is not None:
            return self._normal.avg()
        # case 2 -> (sparse)
        elif self._sparse is not None:
            return self._sparse.avg()
        else:
            # not any case, throw error
            raise Exception("Argument can be Matrix or SparseMatrix object")

    def to_csv(self, path: str, item_spliter=",", row_spliter="\n") -> None:
        # case 1 -> (normal)
        if self._normal is not None:
            self._normal.to_csv(path, item_spliter, row_spliter)
        # case 2 -> (sparse)
        elif self._sparse is not None:
            self._sparse.to_csv(path)
        else:
            # not any case, throw error
            raise Exception("Argument can be Matrix or SparseMatrix object")
    # ---> operations <--- #
