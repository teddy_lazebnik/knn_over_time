# library imports
import os
import math
import numpy

# project imports
from math_ops.math_objects.matrix import Matrix


class SparseMatrix:
    """ A mathematical matrix object """

    def __init__(self, dim1: int, dim2: int, data: list):
        # make sure inputs are legit
        if not (isinstance(dim1, int) and isinstance(dim2, int) and isinstance(data, list) and ((len(data) > 0
                and isinstance(data[0], list) and len(data[0]) == _SparseValue.list_size_to_convert) or
                                                                                                len(data) == 0) or
                (len(data) > 0 and isinstance(data[0], _SparseValue))):
            raise Exception("Arguments are not valid")
        if dim1 >= 1 and dim2 >= 1:
            self._row_dim = dim1
            self._col_dim = dim2
        else:
            raise Exception("Dim of the matrix must be integer bigger then 1")

        # try to assign data
        if not self.assign(matrix_data=data):
            # defaults data
            self._data = []

    @staticmethod
    def init(data: list):
        """ hard init matrix object from raw data as a 2d-array"""
        # make sure inputs are legit
        if not (isinstance(data, list) and
                len(data) > 0 and
                isinstance(data[0], list) and
                len(data[0]) == _SparseValue.list_size_to_convert and
                all([type(data[0][i]) in [int, float] for i in range(_SparseValue.list_size_to_convert)])):
            raise Exception("Arguments are not valid")
        row_dim = max([item[0] for item in data]) + 1
        col_dim = max([item[1] for item in data]) + 1
        # build object
        answer = SparseMatrix(dim1=row_dim,
                              dim2=col_dim,
                              data=data)
        return answer

    # ---> operations <--- #

    def same_shape(self, matrix) -> bool:
        """ check if the matrix is the same shape as this one """
        # check if we are fine to operate
        if not (isinstance(matrix, SparseMatrix)):
            raise Exception("Matrix shape must agree")
        return True if self._row_dim == matrix.dim_1() and self._col_dim == matrix.dim_2() else False

    def scalar(self, scalar: float):
        """ mult by scalar the matrix """
        if type(scalar) in [int, float]:
            # in this case all values are 0 and so we can forget about them
            if scalar == 0:
                return SparseMatrix(dim1=self.dim_1(),
                                    dim2=self.dim_2(),
                                    data=[])
            return SparseMatrix(dim1=self.dim_1(),
                                dim2=self.dim_2(),
                                data=[_SparseValue(i=point.i,
                                                   j=point.j,
                                                   value=scalar * point.value)
                                      for point in self._data])
        raise Exception("scalar is int or float")

    def add(self, matrix):
        """ add the other matrix with this one """
        # check if we are fine to operate
        if not (isinstance(matrix, SparseMatrix) and self.same_shape(matrix=matrix) and len(matrix._data) > 0):
            raise Exception("Matrix shape must agree")
        # error value
        error_value = -1
        # add sparse matrix
        answer = []
        our_values = self._data.copy()
        our_values.extend(matrix._data.copy())
        for value_index_1 in range(len(our_values)):
            flag_added = False
            for value_index_2 in range(value_index_1+1, len(our_values)):
                if _SparseValue.same_position(our_values[value_index_1], our_values[value_index_2]):
                    temp = our_values[value_index_1]
                    if temp.value + our_values[value_index_2].value != 0:
                        answer.append(_SparseValue(i=temp.i,
                                                   j=temp.j,
                                                   value=temp.value + our_values[value_index_2].value))
                    # that we won't find it 2 times
                    our_values[value_index_2].i = error_value
                    flag_added = True
            if not flag_added and our_values[value_index_1].i != error_value:
                temp = our_values[value_index_1]
                answer.append(_SparseValue(i=temp.i,
                                           j=temp.j,
                                           value=temp.value))
        return SparseMatrix(dim1=self.dim_1(),
                            dim2=self.dim_2(),
                            data=answer)

    def sub(self, matrix):
        """ sub the other matrix with this one """
        return self.add(matrix.scalar(-1))

    def mult(self, matrix):
        """
        mult the 2 matrix:
        Addition operation traverses the matrices linearly, hence, has a time complexity of O(n),
        where n is the number of non-zero elements in the larger matrix amongst the two.
        Transpose has a time complexity of O(n+m), where n is the number of columns and m is the number of
        non-zero elements in the matrix. Multiplication, however, has a time complexity of O(x*n + y*m),
        where (x, m) is number of columns and terms in the second matrix;
        and (y, n) is number of rows and terms in the first matrix."""
        # make sure we can mult this 2 matrices
        if not (self.dim_2() == matrix.dim_1()):
            raise Exception("Matrix dims must agree")

        # clac mult
        matrix = matrix.transpose()
        # answer matrix of dimension self.dim1 X matrix.dim2
        # however b has been transposed, hence self.dim1 X matrix.dim1
        answer = SparseMatrix(dim1=self.dim_1(),
                              dim2=matrix.dim_1(),
                              data=[])
        # run over the data points
        for apos in range(len(self._data)):
            # correct row of answer matrix
            r = self._data[apos].i
            # run over the result matrix
            for bpos in range(len(matrix._data)):
                # current col of answer matrix
                c = matrix._data[bpos].i
                # temporary pointers created to add all multiplied values to obtain current element of answer matrix
                temp_a = apos
                temp_b = bpos
                sum = 0
                # iterate over all elements with  same row and col value to calculate answer[r]
                while temp_a < len(self._data) and self._data[temp_a].i == r and \
                    temp_b < len(matrix._data) and matrix._data[temp_b].i == c:
                    if self._data[temp_a].j < matrix._data[temp_b].j:
                        # skip a
                        temp_a += 1
                    elif self._data[temp_a].j > matrix._data[temp_b].j:
                        temp_b += 1
                    else:
                        sum += self._data[temp_a].value * matrix._data[temp_b].value
                        temp_a += 1
                        temp_b += 1
                # insert sum obtained in answer[r] if its not equal to 0
                if sum != 0:
                    answer.set(r, c, sum)

                while bpos < len(matrix._data) and matrix._data[bpos].i == c:
                    # jump to next col
                    bpos += 1

            while apos < len(self._data) and self._data[apos].i == r:
                # jump to next col
                apos += 1
        return answer

    def transpose(self):
        """ transpose this matrix """
        return SparseMatrix(dim1=self.dim_2(),
                            dim2=self.dim_1(),
                            data=[_SparseValue(i=point.j,
                                               j=point.i,
                                               value=point.value)
                                  for point in self._data])

    def tr(self):
        """ return the trace of a given square matrix """
        # check if we are fine to operate
        if not (self.is_square()):
            raise Exception("Matrix must be square")
        return sum([point.value for point in self._data if point.i == point.j])

    def inner(self, matrix):
        """ get the inner product of the 2 matrix """
        return self.mult(matrix=matrix).tr()

    def norm(self, p=2) -> float:
        """ norm of the matrix """
        if type(p) not in [int, str] or \
                (isinstance(p, int) and not (p >= 1 or p in [-1, 0])) or \
                (isinstance(p, str) and p.strip().lower() not in ['sum', 'inf', 'max']):
            raise Exception("Norm P must be integer bigger or equal then 1 or 'sum', 'inf', 'max'")
        if isinstance(p, int) and p == 1:
            return sum([abs(point.value) for point in self._data])
        elif (isinstance(p, int) and p == 0) or (isinstance(p, str) and p == "inf"):
            lines_sum_value = {}
            for point in self._data:
                try:
                    lines_sum_value[point.i] += point.value
                except:
                    lines_sum_value[point.i] = point.value
            return max([lines_sum_value[key] for key in lines_sum_value.keys()])
        elif (isinstance(p, int) and p == -1) or (isinstance(p, str) and p.strip().lower() == "max"):
            return max([point.value for point in self._data])
        elif isinstance(p, str) and p.strip().lower() == "sum":
            return sum([point.value for point in self._data])
        else:
            return math.pow(sum([math.pow(point.value, p) for point in self._data]), 1 / p)

    def is_diagonal(self) -> bool:
        """ check if diagonal matrix """
        for point in self._data:
            if point.i != point.j:
                return False
        return True

    def is_trigonal(self) -> bool:
        """ check if trigonal matrix """
        for point in self._data:
            if point.i > point.j:
                return False
        return True

    def is_jordan(self) -> bool:
        """ check if jordan matrix """
        for point in self._data:
            if not (abs(point.i - point.j) <= 1):
                return False
        return True

    def is_symmetric(self) -> bool:
        """ check if symmetric matrix """
        # TODO: make faster
        # run over all the points in the matrix
        for point1_index in range(int(len(self._data))):
            # foreach point, get the symmetric one
            point_symetric = _SparseValue.get_transposed(self._data[point1_index])
            is_found = False
            # check if we can find the symmetric point in the data
            for point2_index in range(len(self._data)):
                # check if the syymetric point or not
                if _SparseValue.is_same(point_symetric, self._data[point2_index]):
                    is_found = True
                    break
            # check if we could find for this pi
            if not is_found:
                return False
        return True

    # ---> end - operations <--- #

    # ---> meta functions <--- #

    def get(self, i: int, j: int) -> float:
        # check if valid inputs
        for point in self._data:
            if point.position() == (i, j):
                return point.value
            return 0

    def set(self, i: int, j: int, value: float) -> None:
        if value != 0:
            for point in self._data:
                if point.position() == (i, j):
                    point.value = value
                    return
            self._data.append(_SparseValue(i=i, j=j, value=value))

    def assign(self, matrix_data: list) -> bool:
        """ try to update the data of the matrix """
        if (isinstance(matrix_data, list) and
                len(matrix_data) > 0 and
                isinstance(matrix_data[0], list) and
                len(matrix_data[0]) == _SparseValue.list_size_to_convert):
            self._data = [_SparseValue(i=matrix_value[0],
                                       j=matrix_value[1],
                                       value=matrix_value[2])
                          for matrix_value in matrix_data
                          if matrix_value[2] != 0]
            return True
        elif (isinstance(matrix_data, list) and
                len(matrix_data) > 0 and
                all([isinstance(matrix_data[i], _SparseValue) for i in range(len(matrix_data))])):
            self._data = matrix_data.copy()
            return True
        else:
            self._data = []
            return False

    def shape(self) -> tuple:
        return self._row_dim, self._col_dim

    def dim_1(self) -> int:
        return self._row_dim

    def dim_2(self) -> int:
        return self._col_dim

    def size(self) -> int:
        return self._row_dim * self._col_dim

    def is_square(self) -> int:
        return self._row_dim == self._col_dim

    @staticmethod
    def _black_matrix(dim1: int, dim2: int):
        return SparseMatrix(dim1=dim1,
                            dim2=dim2,
                            data=[])

    @staticmethod
    def eye_matrix(dim: int):
        return SparseMatrix(dim1=dim,
                            dim2=dim,
                            data=[_SparseValue(i=i,
                                               j=i,
                                               value=1)
                                  for i in range(dim)])

    def __repr__(self) -> str:
        return "<SparseMatrix: (rows={} | cols={})>".format(self._row_dim, self._col_dim)

    def __str__(self) -> str:
        return "[{}]".format(",".join([point.__str__() for point in self._data]))

    def human_print(self) -> str:
        answer = "SparseMatrix of sizes ({}X{}):\n".format(self.dim_1(), self.dim_2())
        answer += "   Row   Col       Value   \n"
        for point in self._data:
            answer += "   {}     {}          {}\n".format(point.i, point.j, point.value)
        return answer

    # ---> end - meta functions <--- #

    # ---> meta functions <--- #

    def to_csv(self, path: str) -> None:
        """ convert the matrix into csv file """
        # convert sparse matrix to file
        with open(path, "w") as save_file:
            save_file.write("dim1={}\ndim2={}\ndata={}".format(self.dim_1(), self.dim_2(), self._data))

    # ---> end - meta functions <--- #

    # ---> analysis functions <--- #

    def sparse(self) -> float:
        """ calc how much sparse a matrix is """
        size = self.size()
        return 1 - (len(self._data) / size)

    def is_better_normal_matrix_in_memory(self) -> float:
        """ calc how much sparse a matrix is """
        pointer_and_3_values_size = 4
        return self.sparse() > 1 / pointer_and_3_values_size

    def avg(self) -> float:
        """ get the avg(avg(matrix)) value """
        return sum([point.value for point in self._data]) / self.size()

    # ---> end - analysis functions <--- #

    # ---> convert functions <--- #

    def to_matrix(self) -> Matrix:
        """ convert this sparse matrix to a normal matrix representation """
        answer = Matrix(dim1=self.dim_1(),
                        dim2=self.dim_2())
        [answer.set(i=point.i,
                    j=point.j,
                    value=point.value)
         for point in self._data]
        return answer

    @staticmethod
    def from_matrix(matrix: Matrix):
        """ convert a normal matrix representation to sparse matrix representation"""
        return SparseMatrix(dim1=matrix.dim_1(),
                            dim2=matrix.dim_2(),
                            data=[[i, j, matrix._data[i][j]]
                                  for i in range(len(matrix._data))
                                  for j in range(len(matrix._data[i]))
                                  if matrix._data[i][j] != 0])

    # ---> ebd - convert functions <--- #


class _SparseValue:
    """ a class to hold a sparse value in the sparse matrix """

    list_size_to_convert = 3

    def __init__(self, i: int, j: int, value: float):
        # check input is valid
        self.i = i
        self.j = j
        self.value = value

    def __repr__(self) -> str:
        return "<SparseValue: ({},{}) -> {}>".format(self.i, self.j, self.value)

    def __str__(self) -> str:
        return "(i={}, j={}, val={})".format(self.i, self.j, self.value)

    @staticmethod
    def same_position(sv1, sv2) -> bool:
        """ check if 2 values in the same position in the real matrix"""
        return sv1.i == sv2.i and sv1.j == sv2.j

    @staticmethod
    def is_same(sv1, sv2) -> bool:
        """ check if 2 values in the same position in the real matrix"""
        return _SparseValue.same_position(sv1, sv2) and sv1.value == sv2.value

    @staticmethod
    def get_transposed(sv):
        """ get the symetric position to the sparse value """
        return _SparseValue(i=sv.j,
                            j=sv.i,
                            value=sv.value)

    def copy(self):
        """ copy this instance to new one"""
        return _SparseValue(i=self.i,
                            j=self.j,
                            value=self.value)

    def position(self) -> tuple:
        """ get the position of the value"""
        return self.i, self.j
