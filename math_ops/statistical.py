""" Statistical class operations on populations """


# library imports
import math
import statistics


class Statistical:
    """ description """

    # ---> consts <--- #

    # ---> end - consts <--- #

    def __init__(self):
        pass

    @staticmethod
    def range(population: list) -> float:
        if population is not None and isinstance(population, list) and len(population) > 0:
            return max(population) - min(population)
        raise Exception("mean can be done on a list of numbers")

    @staticmethod
    def mean(population: list) -> float:
        if population is not None and isinstance(population, list) and len(population) > 0:
            return sum(population) / len(population)
        raise Exception("mean can be done on a list of numbers")

    @staticmethod
    def rms(population: list) -> float:
        if population is not None and isinstance(population, list) and len(population) > 0:
            return math.sqrt(sum([x * x for x in population]) / len(population))
        raise Exception("mean can be done on a list of numbers")

    @staticmethod
    def median(population: list) -> float:
        if population is not None and isinstance(population, list) and len(population) > 0:
            population = sorted(population)
            if (len(population) % 2) == 1:
                return population[math.floor(len(population)/2)]
            else:
                return (population[math.floor(len(population) / 2)] + population[math.floor(len(population) / 2) + 1])/2
        raise Exception("mean can be done on a list of numbers")

    @staticmethod
    def common(population: list) -> float:
        if population is not None and isinstance(population, list) and len(population) > 0:
            # build histogram
            value_count = Statistical.histogram(population=population)
            # find best
            best = population[0]
            best_count = population[0]
            for value_key in value_count:
                if value_count[value_key] > best_count:
                    best_count = value_count[value_key]
                    best = value_key
            return best
        raise Exception("mean can be done on a list of numbers")

    @staticmethod
    def std(population: list) -> float:
        if population is not None and isinstance(population, list) and len(population) > 1:
            return statistics.stdev(population)
        elif len(population) == 1:
            return 0.0
        raise Exception("mean can be done on a list of numbers")

    @staticmethod
    def var(population: list) -> float:
        if population is not None and isinstance(population, list) and len(population) > 0:
            return math.pow(statistics.stdev(population), 2)
        raise Exception("mean can be done on a list of numbers")

    @staticmethod
    def gaussian_outlier(population: list, std_factor=2) -> list:
        if population is not None and isinstance(population, list) and len(population) > 0:
            mean = Statistical.mean(population)
            std = Statistical.std(population)
            upper_border = mean + std_factor * std
            lower_border = mean - std_factor * std
            return [population_item for population_item in population if lower_border <= population_item <= upper_border]
        raise Exception("mean can be done on a list of numbers")

    @staticmethod
    def histogram(population: list) -> dict:
        if population is not None and isinstance(population, list) and len(population) > 0:
            # build histogram
            value_count = {}
            for value in population:
                try:
                    value_count[value] += 1
                except:
                    value_count[value] = 1
            return value_count
        raise Exception("mean can be done on a list of numbers")

    @staticmethod
    def linear_a_b_to_c_d(population: list, c: float, d: float) -> list:
        """ a - min value of samples, b - max value of samples, c - new min, d - new max
            x -> (x * (d-c) + cb - ad) / (b-a) """
        if population is not None and isinstance(population, list) and len(population) > 0:
            # make sure the [c-d] is right
            if c > d:
                holder = c
                c = d
                d = holder
            a = min(population)
            b = min(population)
            scale = max(population) - a
            new_scale = d - c
            cb_minus_ad = c * b - a * d
            if scale != 0:
                return [(x * new_scale + cb_minus_ad) / scale for x in population]
            # error case for where the list is a list of const
            return [0 for x in population]
        raise Exception("mean can be done on a list of numbers")

    @staticmethod
    def linear_a_b_to_0_1(population: list) -> list:
        """ x -> (x - a) / (b-a) """
        return Statistical.linear_a_b_to_c_d(population=population, c=0, d=1)

    # ---> help functions <--- #

    # ---> end - help functions <--- #
