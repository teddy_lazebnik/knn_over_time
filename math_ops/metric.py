# library imports

# project imports
from math_ops.distances import Distances
from math_ops.math_objects.matrix import Matrix
from math_ops.vector_operation import VectorsOperations


class Metric:
    """ few mathematical metrices functions """

    def __init__(self):
        pass

    @staticmethod
    def scalar_abs_error(val1: float, val2: float) -> float:
        return abs(val1 - val2)

    @staticmethod
    def scalar_relative_error(check: float, gt: float) -> float:
        if check != 0:
            return abs(check - gt) / check
        raise ZeroDivisionError("val1 cannot be 0 in relative error")

    @staticmethod
    def vector_abs_error(vec1: list, vec2: list, p=2) -> float:
        return Distances.norm(vector=VectorsOperations.sub(vec1, vec2),
                              p=p)

    @staticmethod
    def vector_relative_error(vec1: list, vec2: list, p=2) -> float:
        divider = Distances.norm(vec1)
        if divider != 0:
            return Distances.norm(vector=VectorsOperations.sub(vec1, vec2),
                                  p=p) / divider
        raise ZeroDivisionError("||vec1|| cannot be 0 in relative error")

    @staticmethod
    def delta_matrix(matrix1: Matrix, matrix2: Matrix, p=2) -> float:
        """ get the delta size between 2 matrices (absulote error) """
        return matrix1.sub(matrix2).norm(p)

    @staticmethod
    def delta_matrix_relative(matrix1: Matrix, matrix2: Matrix, p=2) -> float:
        """ get the delta size between 2 matrices relative to the first matrix size (relative error) """
        return Metric.delta_matrix(matrix1=matrix1, matrix2=matrix2) / matrix1.norm(p)

    @staticmethod
    def matrix_size_delta(matrix1: Matrix, matrix2: Matrix, p=2) -> float:
        """ get the differences in sizes between 2 matrices"""
        return abs(matrix1.norm(p) - matrix2.norm(p))

    @staticmethod
    def matrix_cosine(matrix1: Matrix, matrix2: Matrix, p=2) -> float:
        """ get the differences in sizes between 2 matrices """
        return matrix1.inner(matrix2) / (matrix1.norm(p) * matrix2.norm(p))

    @staticmethod
    def dissimilarity_matrix(matrics: list, p=2) -> Matrix:
        """ get the dissimilarity matrix between list of matrices """
        answer = Matrix.init([[Metric.delta_matrix_relative(matrix1=matrics[i],
                                                            matrix2=matrics[j],
                                                            p=p) if i < j else 1 if i == j else 0
                               for j in range(len(matrics))]
                              for i in range(len(matrics))])
        return Matrix.make_symmetric(matrix=answer)
